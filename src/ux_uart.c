/*
 * ux_uart.c
 *
 *  Created on: Apr 12, 2017
 *      Author: developer
 */
#include "r_cg_macrodriver.h"
#include "r_cg_serial.h"
#include "ux_uart.h"

typedef enum eu_Status{eTX_BufferBusyBit = 0x01, eRX_BufferBusyBit = 0x02, eRX_BufferAssignedBit = 0x04} eu_Status_t;

static unsigned char u0_TX_Buffer[1];
static unsigned char u0_RX_Buffer[1];
static volatile eu_Status_t   u0_Status = 0;

static unsigned char u1_TX_Buffer[1];
static unsigned char u1_RX_Buffer[1];
static volatile eu_Status_t   u1_Status = 0;

/*
 *
 */
void u0_TxCallBack(void)
{
    u0_Status &= ~eTX_BufferBusyBit;    /* mark buffer as sent */
}
/*
 *
 */
void u0_RxCallBack(void)
{
    u0_Status |=  eRX_BufferBusyBit;    /* mark buffer as full */
}
/*
 *
 */
MD_STATUS u0_Putc(const unsigned char c)
{
    MD_STATUS status;

    while(u0_Status & eTX_BufferBusyBit); /* needs a timeout otherwise the WDT will assert a reset when UART is busy */
    u0_TX_Buffer[0] = c;
    u0_Status |=  eTX_BufferBusyBit;
    status = R_UART0_Send(u0_TX_Buffer, sizeof(u0_TX_Buffer));
    return status;
}
/*
 *
 */
MD_STATUS u0_PutcNonBlocking(const unsigned char c)
{
    MD_STATUS status;

    if (u0_Status & eTX_BufferBusyBit)
    {
        status = MD_TX_BUSY;
    }
    else
    {
        u0_TX_Buffer[0] = c;
        u0_Status |=  eTX_BufferBusyBit;
        status = R_UART0_Send(u0_TX_Buffer, sizeof(u0_TX_Buffer));
    }
    return status;
}
/*
 *
 */
MD_STATUS u0_GetcNonBlocking(unsigned char * c)
{
    MD_STATUS status;

    if (u0_Status & eRX_BufferBusyBit)
    {
        if(c)
        {
            *c = u0_RX_Buffer[0];
        }
        u0_Status &= ~eRX_BufferBusyBit;
        u0_Status &= ~eRX_BufferAssignedBit;
        status = MD_RX_DATA;
    }
    else
    {
        if (!(u0_Status & eRX_BufferAssignedBit))
        {
            u0_Status |= eRX_BufferAssignedBit;
            status = R_UART0_Receive(u0_RX_Buffer, sizeof(u0_RX_Buffer));
        }
        else
        {
            status = MD_RX_WAITING;
        }
    }
    return status;
}
/*
 *
 */
MD_STATUS u0_Puts(const __far unsigned char * c)
{
    MD_STATUS status;
    unsigned char OutByte;

    if(c)
    {
        status = MD_OK;
        OutByte = *c;
        while(OutByte)
        {
            status = u0_Putc(OutByte);
            if(status == MD_OK)
            {
                R_WDT_Restart();
                c++;
                OutByte = *c;
            }
        }
    }
    else
    {
        status = MD_ARGERROR;
    }
    return status;
}
/*
 *
 */
void u1_TxCallBack(void)
{
    u1_Status &= ~eTX_BufferBusyBit;
}
/*
 *
 */
void u1_RxCallBack(void)
{
    u1_Status |=  eRX_BufferBusyBit;
}
/*
 *
 */
MD_STATUS u1_Putc(const unsigned char c)
{
    MD_STATUS status;

    while(u1_Status &  eTX_BufferBusyBit); /* needs a timeout otherwise the WDT will assert a reset when UART is busy */
    u1_TX_Buffer[0] = c;
    u1_Status |=  eTX_BufferBusyBit;
    status = R_UART1_Send(u1_TX_Buffer, sizeof(u1_TX_Buffer));
    return status;
}
/*
 *
 */
MD_STATUS u1_PutcNonBlocking(const unsigned char c)
{
    MD_STATUS status;

    if (u1_Status & eTX_BufferBusyBit)
    {
        status = MD_TX_BUSY;
    }
    else
    {
        u1_TX_Buffer[0] = c;
        u1_Status |=  eTX_BufferBusyBit;
        status = R_UART1_Send(u1_TX_Buffer, sizeof(u1_TX_Buffer));
    }
    return status;
}
/*
 *
 */
MD_STATUS u1_GetcNonBlocking(unsigned char * c)
{
    MD_STATUS status;

    if (u1_Status & eRX_BufferBusyBit)
    {
        if(c)
        {
            *c = u1_RX_Buffer[0];
        }
        u1_Status &= ~eRX_BufferBusyBit;
        u1_Status &= ~eRX_BufferAssignedBit;
        status = MD_RX_DATA;
    }
    else
    {
        if (!(u1_Status & eRX_BufferAssignedBit))
        {
            u1_Status |= eRX_BufferAssignedBit;
            status = R_UART1_Receive(u1_RX_Buffer, sizeof(u1_RX_Buffer));
        }
        else
        {
            status = MD_RX_WAITING;
        }
    }
    return status;
}
/*
 *
 */
MD_STATUS u1_Puts(const __far unsigned char * c)
{
    MD_STATUS status;
    unsigned char OutByte;

    if(c)
    {
        status = MD_OK;
        OutByte = *c;
        while(OutByte)
        {
            status = u1_Putc(OutByte);
            if(status == MD_OK)
            {
                R_WDT_Restart();
                c++;
                OutByte = *c;
            }
        }
    }
    else
    {
        status = MD_ARGERROR;
    }
    return status;
}
