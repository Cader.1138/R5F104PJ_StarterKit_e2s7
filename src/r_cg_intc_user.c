/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_intc_user.c
* Version      : CodeGenerator for RL78/G14 V2.05.02.05 [30 May 2018]
* Device(s)    : R5F104PJ
* Tool-Chain   : GCCRL78
* Description  : This file implements device driver for INTC module.
* Creation Date: 8/11/2018
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_intc.h"
/* Start user code for include. Do not edit comment generated here */
/* Includes user switch and LEDs pin definitions */
#include "rskrl78g14def.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* Switch flag which indicates which switches have been pressed */
volatile uint8_t gSwitch_Flag = 0x00;
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: r_intc8_interrupt
* Description  : This function is INTP8 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_intc8_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* Set the switch flag to indicate a SW1 switch press */
    gSwitch_Flag = SWITCHPRESS_1;

    /* Clear interrupt flag */
    PIF8 = 0;
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_intc9_interrupt
* Description  : This function is INTP9 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_intc9_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* Set the switch flag to indicate a SW2 switch press */
    gSwitch_Flag = SWITCHPRESS_2;

    /* Clear interrupt flag */
    PIF9 = 0;
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_intc10_interrupt
* Description  : This function is INTP10 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_intc10_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* Set the switch flag to indicate a SW3 switch press */
    gSwitch_Flag = SWITCHPRESS_3;

    /* Clear interrupt flag */
    PIF10 = 0;
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/***********************************************************************************************************************
* Function Name : FlashLEDs
* Description   : Flashes the user LEDs on the RSK
* Argument      : none
* Return value  : none
***********************************************************************************************************************/
void Flash_LEDs(void)
{
    /* Declare loop iteration count variable.
       Declared as volatile to prevent optimisation when compiled
       and executed using the Release build configuration  */
    volatile uint8_t flash_count=0;

    /* The GCC compiler C run time
     * appears to have a bug in the
     * initializer for gSwitchFlag.
     *
     * In the debug build gSwitch_Flag is set to 0xFF
     */
    gSwitch_Flag = 0;

    /* Enable SW1 interrupts */
    R_INTC8_Start();

    /* Enable SW2 interrupts */
    R_INTC9_Start();

    /* Enable SW3 interrupts */
    R_INTC10_Start();

    /* Flash the LEDs for 200 times or until a user switch is pressed. */
    while((0 == gSwitch_Flag) && (flash_count++ < 200))
    {
        unsigned long T0_LED_delay;

#define LED_WAIT_TICKS (512ul)

        T0_LED_delay = GetSystemTicks();
        /* LED flashing Delay */
        while(GetSystemTicks() - T0_LED_delay < LED_WAIT_TICKS)
        {
            R_WDT_Restart();
        }

        /* Toggles the LEDs after a specific delay. */
        Toggle_LEDs();
    }

    /* Disable SW1 interrupts */
    R_INTC8_Stop();

    /* Disable SW2 interrupts */
    R_INTC9_Stop();

    /* Disable SW3 interrupts */
    R_INTC10_Stop();

    /* Reset the flag variable */
    gSwitch_Flag = 0;

}
/***********************************************************************************************************************
End of function FlashLEDs
***********************************************************************************************************************/

/***********************************************************************************************************************
* Function Name : Toggle_LEDs
* Description   : Toggles the LEDs on the RSK
* Argument      : none
* Return value  : none
***********************************************************************************************************************/
void Toggle_LEDs(void)
{
    unsigned long T0_LED_delay;

    T0_LED_delay = GetSystemTicks();
    /*  Toggle the state of the user LEDs  */
    LED0 = ~LED0;
    while(GetSystemTicks() - T0_LED_delay < LED_WAIT_TICKS - (LED_WAIT_TICKS*3/4))
    {
        R_WDT_Restart();
    }
    LED1 = ~LED1;
    while(GetSystemTicks() - T0_LED_delay < LED_WAIT_TICKS - (LED_WAIT_TICKS*2/4))
    {
        R_WDT_Restart();
    }
    LED2 = ~LED2;
    while(GetSystemTicks() - T0_LED_delay < LED_WAIT_TICKS - (LED_WAIT_TICKS*1/4))
    {
        R_WDT_Restart();
    }
    LED3 = ~LED3;
}
/***********************************************************************************************************************
End of function ToggleLEDs
***********************************************************************************************************************/

/* End user code. Do not edit comment generated here */
