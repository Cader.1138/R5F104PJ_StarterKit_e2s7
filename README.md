﻿# R5F104PJ_StarterKit_e2s7

Renesas Developer Kit, Starter Kit P/N: YR0K50104PS000BE demo code using e²studio 7.0.0 and gcc-4.9.2.201801-GNURL78-ELFF tool chain.

This repository is in a raw state. The content builds using Renesas e²studio 7.0.0 and the GCC compiler from https://gcc-renesas.com version 4.9.2.201801.

The present code configures the maximum number of PWM outputs with independent duty cycle controls, square wave generator output, two buzzer outputs, RTC 1Hz square wave output. It total 14 digital outputs.

In addition behavior like that in the starter kit tutorial sample code is implemented. 

The LEDs run a chase cycle for awhile, then messages are displayed on the LCD module, then LED begin flashing controlled by the position of the potentiometer sampled by the ADC. When the test mode changes diagnostic messages are sent to the DB9 RS232 channel (UART1, 9600 baud, 8-bits, No parity, 1 stop bit).

The test cycle ends with an echo test. Typing a CTRL-C will begins a watchdog timeout test sequence that ends with a WDT reset and the test cycle restarts.
